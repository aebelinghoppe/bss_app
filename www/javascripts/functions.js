

  function make_base_auth(user, password) {
     if ((user != null) && (password != null)){
       var user = rc4(key(), user);
       var password = rc4(key(), password);
     }
    var tok = user + ':' + password;
    var hash = btoa(tok);
    return "Basic " + hash;
  } 

  function pad(number, length) {   
    var str = '';
    str = str + number;
    while (str.length < length) {
      str = '0' + str;
    }  
    return str;
  } 


function initPushwoosh(){
    var pushNotification = window.plugins.pushNotification;
 
    //set push notifications handler
    document.addEventListener('push-notification', function(event) {
        var title = event.notification.title;
        var userData = event.notification.userdata;
                                 
        if(typeof(userData) != "undefined") {
            console.warn('user data: ' + JSON.stringify(userData));
        }
                                     
        alert(title);
    });
     
    pushNotification.onDeviceReady({ projectid: "177700306947", appid : "FDEB1-881D5" });
 
    //register for pushes
    pushNotification.registerDevice(
        function(status) {
            var pushToken = status;
            console.warn('push token: ' + pushToken);
        },
        function(status) {
            console.warn(JSON.stringify(['failed to register ', status]));
        }
    );
}