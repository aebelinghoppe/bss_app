  function post_message(myobject){            
        if (($.isNumeric(myobject.val()) )&&(myobject.val() > -1)){
          window.localStorage.setItem("anfahrtzeit", myobject.val());    
        }else{
          window.localStorage.setItem("status",myobject.val());         
        }
        
            
        var my_status = window.localStorage.getItem("status"); 
        if (my_status == "abwesend"){
          window.localStorage.setItem("anfahrtzeit","0");   
          $('#anfahrtzeit').val(""); 
        }
        if ((my_status == "verfügbar") && (!(window.localStorage.getItem("anfahrtzeit") >= 1))) {
          window.localStorage.setItem("anfahrtzeit","1");   
          $('#anfahrtzeit').val("1"); 
        }
        var anfahrtzeit = window.localStorage.getItem("anfahrtzeit");
        var my_user_data = new Object;
        var dt = new Date();
        var currentTime = pad(dt.getDate(),2) + "." + pad((dt.getMonth()+1),2) +  "." + dt.getFullYear().toString().substr(2,2) + " " + pad(dt.getHours(),2) + ":" + pad(dt.getMinutes(),2) + "Uhr";
        var username = window.localStorage.getItem("user_input");
        var password = window.localStorage.getItem("pwd_input");
        var team = window.localStorage.getItem("team_input");
        var funktion = window.localStorage.getItem("funktion_input");
        var raw_domain = window.localStorage.getItem("url_input");

        if ((username != null) && (password != null) && (raw_domain != null) ){
          var domain = raw_domain.replace("http://","");         
          var uri = "http://" + rc4(key(), username) + ":" + rc4(key(), password) + "@" + domain;
        }
        var user = Object;
        if (username == null){          
          navigator.notification.alert("Bitte Benutzername und Passwort angeben.", null, 'Fehlende Konfiguration', 'OK');
       
          return false;
        }
       // alert(username+" "+password+" " + uri + "/wp-json/users/me?creds="+ rc4(key(), username) + ":" + rc4(key(), password));

        $.ajax({
            type: 'GET',        
            url: uri + "/wp-json/users/me?creds="+ rc4(key(), username) + ":" + rc4(key(), password),
            crossDomain: true,
            //contentType: 'application/javascript; charset=UTF-8',                          
            //dataType: 'json',
            timeout: 30000,
            
            success: function(user) {                 
              if(user != null){                
                if ( user.roles.indexOf("administrator") > -1 ){
                  $("#admin_section").show();
                }
              }   
              var post_url = uri + "/wp-json/users/" + user.ID;   
                                           

              $.ajax({
                type: 'POST',
                url: post_url + "?creds="+ rc4(key(), username) + ":" + rc4(key(), password),
                crossDomain: true,
                //contentType: 'application/javascript; charset=UTF-8',                
                dataType: 'json',
                
                timeout: 30000,
                data: '{"ID":'+user.ID+',"username":"'+username+'", "userstatus": "'+my_status+'", "name":"'+username+'", "updated_at": "'+currentTime+'", "team": "'+team+'", "funktion": "'+funktion+'", "anfahrtzeit": "'+anfahrtzeit+'" }',
                success: function(r) { 
                  //console.log("OK_POST" + JSON.stringify(r));
                },
                error: function(r) { 
                  var antwort = r.responseText || "Der neue Status kann vom Server nicht verarbeitet werden.";
                  navigator.notification.alert( JSON.stringify(antwort).replace(/<[^>]+>[^<]*<[^>]+>|<[^\/]+\/>/ig, ""), null, 'Kommunikationsfehler', 'OK');
                  //console.log("ERR_POST:" +  JSON.stringify(r));
                }
              });
              $("#transmission_alert_success").fadeIn(300);
              $("#transmission_alert_success").fadeOut(1500); 
              $("#current_status").text( my_status + " (" + currentTime + ")");   
                  
            },           
            error: function(r) {      
              var antwort = r.responseText || "keine Verbindung möglich";              
              navigator.notification.alert( JSON.stringify(antwort).replace(/<[^>]+>[^<]*<[^>]+>|<[^\/]+\/>/ig, ""), null, 'Kommunikationsfehler', 'OK');
              return false;
            }           
        });
  } 



$(document).ready(function() {
 document.addEventListener("deviceready", onDeviceReady, false);
});

function onDeviceReady() {    
  // if (device.platform == "android") {
  //   window.plugin.backgroundMode.enable();
  // }
  $( "#anfahrtzeit" ).val(window.localStorage.getItem("anfahrtzeit"));

    //$("#transmission_alert_success").hide();
    //$("#admin_section").hide();

    var username   = window.localStorage.getItem("user_input");    
    var password   = window.localStorage.getItem("pwd_input");
    var raw_domain = window.localStorage.getItem("url_input");
    var user = Object;      
    if ((username != null) && (password != null) && (raw_domain != null) ){
      var domain = raw_domain.replace("http://","");         
      var uri = "http://" + rc4(key(), username) + ":" + rc4(key(), password) + "@" + domain;
      $.ajax({
        type: 'GET',
        url: uri + "/wp-json/users/me?creds="+ rc4(key(), username) + ":" + rc4(key(), password),
        crossDomain: true,
        dataType: 'json',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        timeout: 30000,            
        success: function(user) {   
          if(user != null){                
            if ( user.roles.indexOf("administrator") > -1 ){
              $("#admin_section").show();
            } 
            if (user.userstatus.indexOf("null") > -1){
              $("#current_status").text("kein Status gesetzt...");
            }else{
              $("#current_status").text( user.userstatus + " (" + user.updated_at + ")");
            }
          }else{
            $("#current_status").text("Es sind keine Daten vorhanden.");
          }

        },
        error: function (r) {
          var antwort = "Status: " + (r.statusText || "keine Verbindung möglich") + (r.responseText || "") + ", bitte Einstellungen prüfen"  ;
          //$("#current_status").text("Kommunikationsfehler! " + JSON.stringify(antwort).replace(/<[^>]+>[^<]*<[^>]+>|<[^\/]+\/>/ig, ''));
          $("#current_status").text("");
          navigator.notification.alert( JSON.stringify(antwort).replace(/<[^>]+>[^<]*<[^>]+>|<[^\/]+\/>/ig, ""), null, 'Kommunikationsfehler', 'OK');

        }
      });
    }
    if((username == null) || (username.length < 1) || (password == null) || (password.length < 1) || (raw_domain == null) || (raw_domain.length < 1)){            
      navigator.notification.alert("Bitte konfigurieren Sie zunächt Ihre Verbindung unter Konfig.", null, 'Fehlende Konfiguration', 'OK');
    }
    
    
    
   

    $('.status_btn').click(function(e){    
        post_message($(this));

    });
    
    $('#anfahrtzeit').change(function(e){    
      post_message($(this));
    });

    $('.reset_btn').click(function(e){ 
        $.ajax({
            type: 'GET',        
            url: uri + "/wp-json/users/reset_status?creds="+ rc4(key(), username) + ":" + rc4(key(), password),
            crossDomain: true,
            contentType: 'application/javascript; charset=UTF-8',                
          
            dataType: 'json',
            timeout: 60000,
            
            success: function(user) {                 
              $("#transmission_alert_success").fadeIn(500);
              $("#transmission_alert_success").fadeOut(1000); 
              $("#current_status").text("Es wurde alles zurückgesetzt.");                                   
            }
        });
    });   

}