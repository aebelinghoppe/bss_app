  function get_status(){    
    var data = Object;
    data.username   = window.localStorage.getItem("user_input");    
    data.password   = window.localStorage.getItem("pwd_input");
    var raw_domain = window.localStorage.getItem("url_input");
    if ((data.username != null) && (data.password != null) && (raw_domain != null) ){
        data.domain = raw_domain.replace("http://","");         
        data.uri = "http://" + rc4(key(), data.username) + ":" + rc4(key(), data.password) + "@" + data.domain;
    }else{
      return false;
    }
    $('.error_msg').text('');


    var uri = data.uri;
    var username = data.username;  
    var password = data.password;    
    $.ajax({
          type: 'GET',
          url: uri + "/wp-json/users/status?creds="+ rc4(key(), data.username) + ":" + rc4(key(), data.password),
          crossDomain: true,
          dataType: 'json',
          contentType: 'application/x-www-form-urlencoded; charset=UTF-8',      
          timeout: 30000,            
          success: function(status) {
            $('.list-row').remove();
            if (status.length > 0){
              $.each( status, function( i, element ) {                 
                if(element.anfahrtzeit != null){
                  var anfahrtzeit_i = parseInt(element.anfahrtzeit);
                }else{
                  anfahrtzeit_i = 0;
                }                   
                if ((anfahrtzeit_i <= 1)&&(element.status == "verfügbar")){
                  var anfahrtzeit = "sofort einsatzbereit";
                } else if ((anfahrtzeit_i <= 1)&&(element.status == "teilweise verfügbar")){
                  var anfahrtzeit = "Einsatzbereitschaft unklar";
                } else if ((anfahrtzeit_i <= 1)&&(element.status == "abwesend")){
                  var anfahrtzeit = "abwesend";
                } else if ((anfahrtzeit_i > 1)&&(anfahrtzeit_i < 90)){
                  var anfahrtzeit =  anfahrtzeit_i + " Min am Einsatzort";
                } else if (anfahrtzeit_i == 90){
                  var anfahrtzeit = "1½ Std am Einsatzort";
                } else if (anfahrtzeit_i == 105){
                  var anfahrtzeit = "1¾ Std am Einsatzort";
                } else if (anfahrtzeit_i == 120){
                  var anfahrtzeit = "2 Std am Einsatzort"; 
                }                        
                if (element.status == "verfügbar"){
                  var side_color  = "#46ca5e"; 
                  var img_status = "verfuegbar";
                }else if (element.status == "teilweise verfügbar"){
                  var side_color  = "#FFC400";  
                  var img_status = "teilweise_verfuegbar";
                }else{
                  var side_color  = "#fd3c31";
                  var img_status = "abwesend";
                }
                $(".list-messages").append( "<li class='list-row list-message' style='border-left: 6px solid "+ side_color +" ;'><h3 class='list-messsage-title'>" + element.full_name + " </h3><div class='list-message-date '><p class='small'>zuletzt aktualisiert:</p> " + element.updated_at + "<p class='small'>Gruppe:</p>"  + element.team +  "</div><p class='list-message-anfahrzeit list-"+ img_status +"'>" +anfahrtzeit +" </p><p class='list-message-desc'> "+ element.funktion + "</p></li>" );
              });
            }else{
              $(".list-messages").append( "<p class='list-row'>Aktuell ist kein Einsatz. Daher sind keine Statusinformationen vorhanden.</p>" );
            }
            console.log(JSON.stringify(status));  
          },
          error: function (r) {
            $('.list-row').remove();
            var antwort = r.responseText || "keine Verbindung möglich";
            $(".error_msg").html('Es konnten keine Daten vom Server empfangen werden. <br><p style="font-size:8px;">Fehlermeldung: ' + JSON.stringify(antwort).replace(/<[^>]+>[^<]*<[^>]+>|<[^\/]+\/>/ig, "") + "</p> ");
          }
      });
  }
  


function myTimer() {
    get_status();
    var intervall = (window.localStorage.getItem("intervall_input") || 30)*1000;
    window.setTimeout(myTimer, intervall); 
} 

$(document).ready(function() {
    // are we running in native app or in a browser?
    window.isphone = false;
    if(document.URL.indexOf("http://") === -1 
        && document.URL.indexOf("https://") === -1) {
        window.isphone = true;
    }

    if( window.isphone ) {
        document.addEventListener("deviceready", onDeviceReady, false);
    } else {
        onDeviceReady();
    }
});
          
      


function onDeviceReady() {
    document.addEventListener("visibilitychange", get_status(), false);
    $("#transmission_alert_success").hide();
    setTimeout(function() {
          $('.init_info').hide();   
    }, 15000);
    window.setTimeout(myTimer, (window.localStorage.getItem("intervall_input") || 30)*1000);
    
    $('#hook').hook({
      reloadPage: false,
      reloadEl: function(){
        $('.error_msg').text('');
        get_status();
      }
    });
}
