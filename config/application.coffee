# For an explanation of the steroids.config properties, see the guide at
# http://guides.appgyver.com/steroids/guides/project_configuration/config-application-coffee/

steroids.config.name = "bss_app"

# -- Initial Location --
steroids.config.location = "http://localhost/index.html"

#-- Tab Bar --
steroids.config.tabBar.enabled = true
steroids.config.tabBar.tabs = [
  {
    title: "Mein Status"
    icon: "icons/user-icon.png"
    location: "http://localhost/index.html"
  },
  {
    title: "Alarm"
    icon: "icons/alarm.png"
    location: "http://localhost/alarm.html"
  },
  {
    title: "Übersicht"
    icon: "icons/users-icon.png"
    location: "http://localhost/groups.html"
  },
  {         
    title: "Konfig"
    icon: "icons/settings-icon.png"
    location: "http://localhost/settings2.html"
  }
]

steroids.config.tabBar.tintColor = "#000000"
steroids.config.tabBar.tabTitleColor = "#ffffff"
steroids.config.tabBar.selectedTabTintColor = "#00aeef"
#steroids.config.tabBar.selectedTabBackgroundImage = "icons/pill@2x.png"

# steroids.config.tabBar.backgroundImage = ""

# -- Navigation Bar --
steroids.config.navigationBar.tintColor = "#e7411d"
steroids.config.navigationBar.titleColor = "#FFFFFF"
steroids.config.navigationBar.buttonTintColor = "#FFFFFF"

# steroids.config.navigationBar.landscape.backgroundImage = ""
# steroids.config.navigationBar.portrait.backgroundImage = ""

# -- Android Loading Screen
steroids.config.loadingScreen.tintColor = "#ED1800"

# -- iOS Status Bar --
steroids.config.statusBar.enabled = true
steroids.config.statusBar.style = "default"

# -- File Watcher --
# steroids.config.watch.exclude = ["www/my_excluded_file.js", "www/my_excluded_dir"]

# -- Pre- and Post-Make hooks --
# steroids.config.hooks.preMake.cmd = "echo"
# steroids.config.hooks.preMake.args = ["running yeoman"]
# steroids.config.hooks.postMake.cmd = "echo"
# steroids.config.hooks.postMake.args = ["cleaning up files"]

# -- Default Editor --
# steroids.config.editor.cmd = "subl"
# steroids.config.editor.args = ["."]
